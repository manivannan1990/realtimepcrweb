var cnn; 
function initDatabase() { 
	try {
		if (!window.openDatabase) {
			alert('Local Databases are not supported by your browser. Please use a Webkit browser for this demo');
		} else {	    
			var shortName = 'Realtime';
			var version = '1.0';
			var displayName = 'Realtime';
			var maxSize = 100000; // in bytes	  
			cnn = openDatabase(shortName, version, displayName, maxSize);

			create_bookmark_table();
		}
	} catch(e) {
		if (e == 2) {
		// Version mismatch.
			console.log("Invalid database version");
		} else {
			console.log("Unknown error "+ e +".");
		}
		return;
	} 
}

function create_bookmark_table(){ 
	cnn.transaction(
		function (transaction) { 
			transaction.executeSql('CREATE TABLE IF NOT EXISTS bookmark(id INTEGER PRIMARY KEY AUTOINCREMENT,category TEXT,page TEXT,title TEXT);', [], nullDataHandler, errorHandler_book);

		}
	);
}

function errorHandler_book(){
}

function nullDataHandler(){
}

var category;
var pagename;
var pagetitle;
var lastClickTime = 0;

function prepopulate_Bookmark(cat,page,title){ 
	var current = new Date().getTime(); 
	var delta = current - lastClickTime; 
	if (delta < 800) {
	} else {
		category=cat;  
		if(cat=='Video'){
			pagename=page.replace (/\+/g,"");
			pagetitle=title.replace (/\+/g, "");
		}else{      
			pagename=page;
			pagetitle=title;
		}
		
		console.log("ENTERED HERE");
		var sqlres = "SELECT  *  FROM bookmark where  category='"+cat+"' and page='"+pagename+"'";//me;
		cnn.transaction(
			function (transaction) { 
				transaction.executeSql(sqlres, [],duplication,errorHandler);
			}
		);
	}
	
	window.lastClickTime = current;
}

function errorHandler(){
	alert('failed query');	
}

function sqlstr(s) {
	return "'"+s.replace(/'/g, "''")+"'";
}

function duplication(transaction, results){  
	if(results.rows.length>0){
		alert('This page is already added to bookmark list');
	}else{
		cnn.transaction(
			function (transaction) {
				transaction.executeSql('INSERT INTO bookmark(category,page,title) VALUES('+sqlstr(category)+','+sqlstr(pagename)+','+sqlstr(pagetitle)+')');
				alert('This page is added to bookmark list');	
			}
		);	 
	} 
}


function removePage(cate,pag){ 
	cnn.transaction(
		function (transaction) {
			transaction.executeSql("Delete  from bookmark WHERE category='"+cate+"' and page='"+pag+"' ", [], nullDataHandler, errorHandler);
		}
	);

	if(cate=='Video')
		showVideoBookmarks();
	else if(cate=='Handbook')
		showBookmarks();
	else if(cate=='Application')
		showApplications();
	else
		showTroubleBookmarks();
}

function showSubmenu(path){
	window.location=path;
}

function onBodyTitle(titletxt,prevPageName){		
	$("#headtxt").css({ width: '0px' });
	$('#headtxt').html("");
	
	if((localStorage.mainpage=='handbook') && (($(window).width()>320) && eval($(window).width()) <=eval(640))){ 
		$("#headtxt").css({ width: '70%' });
		$("#headtxt").css('text-align','center');
		var subtitle=titletxt.substr(0,40);
		$('#headtxt').text(titletxt);
	}else if(eval($(window).width()) <= eval(320)){  
		$("#headtxt").css({ width: '40%' });
		$("#headtxt").css('text-align','center');
		var subtitle=titletxt.substr(0,15);
		$('#headtxt').text(subtitle+"..");
	}else if((localStorage.mainpage=='handbook') && (eval($(window).width()) >=eval(640))){  
		$("#headtxt").css({ width: '80%' });
		$("#headtxt").css('text-align','center');
	}

	if((localStorage.mainpage=='search') || (localStorage.mainpage=='video') || (localStorage.mainpage=='videocategorydetail') || (localStorage.mainpage=='bookmark') || (localStorage.mainpage=='calculator') || (localStorage.mainpage=='handbook') || (localStorage.mainpage=='application') || (localStorage.mainpage=='Gene Expression') || (localStorage.mainpage=='Gene Regulation') || (localStorage.mainpage=='Genetic Variation') || (localStorage.mainpage=='results') || (localStorage.mainpage=='details') ||(localStorage.mainpage=='Publications') ||(localStorage.mainpage=='publicationscategory') || (localStorage.mainpage=='troubleshooting')  || (localStorage.mainpage=='Documents') || (localStorage.mainpage=='referencecategorydetail') || (localStorage.mainpage=='search_app')){
			$('#headtxt').text(titletxt);
		if(eval($(window).width()) <= eval(320) && (localStorage.mainpage=='troubleshooting')){
			$("#headtxt").css({ width: '40%' });
		}
		else{
			$("#headtxt").css({ width: '60%' });
		}
		
		$("#headtxt").css('text-align','center');
	}
	
	localStorage.prev=prevPageName;		             
}

function gotoBack(){
	if(localStorage.mainpage=='troubleshooting' || localStorage.mainpage=='application' || localStorage.mainpage=='calculator' ){			
		prevpage=localStorage.prev;
		window.location=prevpage;
	}else if(localStorage.mainpage=='handbook'){  
		var w = window.innerWidth;
		var h = window.innerHeight;

		if(h > w){
			if(h < 800){
				window.location = "../hanbook_iphone.html";
			}
			else{
				window.location = "../hanbook.html";
			}
		}

		if(h < w){
			if( h >= 800){
				window.location = "../hanbook.html";	
			}else{
				window.location = "../hanbook_iphone.html";
			}
		}
	}
	
	else if(localStorage.mainpage=='bookmark'){ 
		var url=window.location.href.substring(0,window.location.href.lastIndexOf('/')); 
		window.location=url.substring(0,url.lastIndexOf('/'))+"/bookmark.html";
	}
	
	else if(localStorage.mainpage=='bookmark_app'){
		var url=window.location.href.substring(0,window.location.href.lastIndexOf('/'));
		window.location=url.substring(0,url.lastIndexOf('/'))+"/www/bookmark.html";
	}
	
	else if(localStorage.mainpage=='search'){ 
		var url=window.location.href.substring(0,window.location.href.lastIndexOf('/'));
		var w=$('body').width();
		
		if(w < 750){              
			var temp=url.substring(0,url.lastIndexOf('/'))+"/search.html?cname="+localStorage.searchString;	
			window.location=url.substring(0,url.lastIndexOf('/'))+"/search.html?cname="+localStorage.searchString;				
		}else{ 
			window.location=url.substring(0,url.lastIndexOf('/'))+"/search.html?cname="+localStorage.searchString;
			var temp=url.substring(0,url.lastIndexOf('/'))+"/search.html?cname="+localStorage.searchString;	
		}
	}
	
	else if(localStorage.mainpage=='search_app'){	 
		var url=window.location.href.substring(0,window.location.href.lastIndexOf('/'));
		window.location=url.substring(0,url.lastIndexOf('/'))+"/www/search.html?cname="+localStorage.searchString;
	} 	
}

function loadiframe(url,backurl){
	progressimg();
	localStorage.exlink=url;
	localStorage.backurl=backurl;
	window.location = "../qpcriframe.html";
}

function progressimg(){
	var htm = '<div class="progresspushdiv"><p style="text-align:center;width:100%;line-height:50px;"><span>Loading...</span>&nbsp;&nbsp;&nbsp;<span><img src="../images/progress.gif"/></span></p></div>';
	$('body').append(htm);
	setTimeout(function(){
		$('.progresspushdiv').remove();
	},5000);
}

function goHome(){
	window.location = "../first.html";
}

function goPrev(url){
	setTimeout(function() {	
		window.location = url;
	}, 500);
}

function goNext(url){
	setTimeout(function() {
		window.location = url;
	}, 500);
}

function goBack(){ 
	prevpage=localStorage.prev;
	window.location=prevpage;
}            